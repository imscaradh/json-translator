# JSON value translator

This simple Python script translates JSON values with help of Google Translator API. 

To use the script, create a venv and install the dependencies with pip:

    (venv)$ python -m venv venv
    (venv)$ pip install -r requirements.txt

Usage:

    translate.py [-h] [-f FILE] [--from FROM_LANG] [--to TO_LANG]

    Translate JSON values with Google Translator API

    optional arguments:
    -h, --help            show this help message and exit
    -f FILE, --json-file FILE
                            json file to translate
    --from FROM_LANG      Original language
    --to TO_LANG          Destination language



Example:

    (venv)$ python translate.py -f ./deutsch.json --from DE --to EN
