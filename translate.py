#!/bin/python3

import sys
import json
import dpath
import pprint
import argparse
from googletrans import Translator

def translate_json(file, from_lang, to_lang):
    # Intialize Google Translator
    translator = Translator()
    
    # Read json file
    with open(file) as f:
        data = json.load(f)

    # Iterate over values and translate them
    def iterate(d, path=""):
      for k, v in d.items():
        new_path = "{}/{}".format(path, k)
        if isinstance(v, dict):
          iterate(v, new_path)
        else:
          translation = translator.translate(v, dest=to_lang.upper(), src=from_lang.upper())
          dpath.util.set(data, new_path[1:], translation.text)

    iterate(data)
    print(json.dumps(data, ensure_ascii=False, indent=2))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Translate JSON values with Google Translator API')
    parser.add_argument('-f', '--json-file', dest='file', help='json file to translate')
    parser.add_argument('--from', dest='from_lang', help='Original language')
    parser.add_argument('--to', dest='to_lang', help='Destination language')

    args = parser.parse_args()
    translate_json(args.file, args.from_lang, args.to_lang)

